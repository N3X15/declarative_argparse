# 0.0.9 - IN DEVELOPMENT

*TBD*

# 0.0.8 - October 14th, 2024

- Fixed `*flags` typehints.
- Added Argument Group support.
- Updated dependencies.

# 0.0.7 - January 9th, 2024

- Added the following to `DeclarativeOptionParser`:
  - `.addSubparser()`
  - `.printHelp()`
  - `.printUsage()`
  - `.setDefaults()`
- Updated dependencies.

# 0.0.6 - April 25, 2023

- Added `addPath()` and `addPathArray()` to `DeclarativeOptionParser`
- Dependencies update

# 0.0.4 - June 16, 2022

- Hotfix for a crash.

# 0.0.3 - June 16, 2022

- Added support for positional arguments.

# 0.0.2 - June 15, 2022

- `add*` now takes at least one long form and zero to many short forms, to comply with argparse API.
- Added `addStoreTrue` and `addStoreFalse`
